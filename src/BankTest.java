import static org.junit.Assert.*;

import org.junit.Test;

public class BankTest {

	@Test
	public void testCreateANewBank() {
		//B1: When a new Bank is created, it has 0 customers.
		//1. Create a new bank account
		Bank bank = new Bank();
        //2. Get number of customers
        int NumberOfCustomers = bank.getNumberOfCustomers();
        //3.Assert
        assertEquals(0,NumberOfCustomers);    
 }
	@Test
	public void testAddCustomers() {
		//B2: The bank can add customers.
		//When a customer is added,The number of Bank customers increases by 1.
		//1. Create a new bank account
		Bank bank = new Bank();
        //2. Add customers to the bank
		bank.addCustomer("Japneet",500);
		bank.addCustomer("Jasveer",600);
		bank.addCustomer("Lovepreet",700);
		//3.Get total number of customers
        int NumberOfCustomers = bank.getNumberOfCustomers();
        //4.Assert
        assertEquals(3,NumberOfCustomers);    
 }
	@Test
	public void testRemoveCustomers() {
		//B3:  The bank can remove customers.
		//When a customer is removed,the number of Bank customers decreases by 1.
		//1. Create a new bank account
		Bank bank = new Bank();
        //2. Add customers to the bank
		bank.addCustomer("Japneet",500);
		bank.addCustomer("Jasveer",600);
		bank.addCustomer("Lovepreet",700);
		//3.Get total number of customers
        int NumberOfCustomers = bank.getNumberOfCustomers();
        //4. Remove customer from the bank
        bank.removeCustomer("Lovepreet");
        //5.Assert
        assertEquals(3,NumberOfCustomers);  
  }  
	@Test
	public void testTransferMoney() {
		//B4: The bank can transfer money between two accounts.
		//The bank identifies accounts based on the Customer name associated with the account.
		//1. Create a new bank account
		Bank bank = new Bank();
        //2. Add customers to the bank
		bank.addCustomer("Japneet",200);
		bank.addCustomer("Jasveer",50);
        //3. Create new account to the bank
        Account FirstAccount =  FirstCustomer.getAccount();
    	//4.creating another new account to the bank
    	Account SecondAccount = SecondCustomer.getAccount();
    	//5.Storing the initial balance
    	int newBalance1 = FirstAccount.balance();
    	int newBalance2 = SecondAccount.balance();
    	//6.Checking the new balance
    	assertEquals(200,newBalance1);
    	assertEquals(10,newBalance2);
    	//7.Transfer the money in the account
    	bank.transferMoney(FirstCustomer,SecondCustomer,50);
    	//8. Storing balance in account
    	int updateBalance1 = FirstAccount.balance();
    	int updateBalance2 = SecondAccount.balance();
    	//9. Checking the new balance
    	assertEquals(150,newBalance1);
    	assertEquals(60,newBalance2);
   }  
}

