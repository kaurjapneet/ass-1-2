import static org.junit.Assert.*;

import org.junit.Test;

public class CustomerTest {


	@Test
	public void testEveryCustomerAccount() {
		//C1: Every customer has an account.
		//1. Create a new customer
		Customer customer = new Customer("Navpreet",800);
		//2. Get customer account name
		String AccountName = customer.getName();
		//3.Assert
		assertEquals("Navpreet",AccountName);
   }
	@Test
	public void testInitialDepositAccount() {
		//C2: New customers can choose to make an optional initial deposit into their account. 
		//If the customer chooses to deposit at the time of account creation, the customer starts with that amount.
		//Otherwise, the customer starts with $0 in the account.
		//1. Create a new customer
		Customer customer = new Customer("Gurpreet",600);
		//2. Get customer account name
		Account account = customer.getAccount();
		//3. Check balance in the account 
		int actualBalance = account.balance();
		//3.Assert
		assertEquals(600,actualBalance);
	}
	@Test
	public void testInitialAccountWithZeroDeposit() {
		//C2: New customers can choose to make an optional initial deposit into their account. 
		//If the customer chooses to deposit at the time of account creation, the customer starts with that amount.
		//Otherwise, the customer starts with $0 in the account.
		//1. Create a new customer
		Customer customer = new Customer("Ramandeep",400);
		//2. Get customer account name
		Account account = customer.getAccount();
		//3. Check balance in the account 
		int actualBalance = account.balance();
		//3.Assert
		assertEquals(0,actualBalance);
	}
}

